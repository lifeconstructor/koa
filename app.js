var koa        = require('koa'),
    app        = koa(),
    views      = require('koa-render'),
    router     = require('./controllers'),
    bodyParser = require('koa-body-parser'),
    json       = require('koa-json')

app.use(json());
app.use(bodyParser());

// TODO: "koa-request-language" package have bugs
// app.use(requestLanguage({
//   languages: ['en-US', 'ru-RU'],
//   cookie: {
//     name: 'language',
//     options: { maxAge: 24*3600*1000 },
//     url: '/languages/{language}'
//   }
// }))
app.use(views('./views', 'pug'));
router(app);

app.listen(3000);
console.log('Listening to port 3000');
