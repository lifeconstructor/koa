# Koa + mongoose + pug

__Koa project template using:__
  
  - [koa](https://www.npmjs.com/package/koa)
  - [pug](https://www.npmjs.com/package/pug)
  - [koa-body-parser](https://www.npmjs.com/package/koa-body-parser)
  - [koa-render](https://www.npmjs.com/package/koa-render)
  - [koa-router](https://www.npmjs.com/package/koa-router)
  - [mongoose](https://www.npmjs.com/package/mongoose)
  - [koa-json](https://www.npmjs.com/package/koa-json)

__Structure:__

  - ` app.js ` is the starting point of the application
  - ` config/ ` mongoose connection to mongodb://localhost:27017
  - ` models/ ` schemas used with mongoose
  - ` controllers/ ` contains the files that handle the @GET requests to a specific url
  - ` views/ ` contains the html files in .pug syntax
  - ` public/ ` contains the static files served by node (currently not used...)

Install dependencies
```
$ npm install
```
 
__Running the application:__
  
Start the application with one of the following:
```
$ npm start 
```
By default the app tries to connect to port 3000. After starting the application you can check it at [localhost:3000](http://localhost:3000)

Start a new mongodb instance
```
$ sudo mongod
```

In a new shell window connect to the mongodb instance and choose database
```
$ mongo
```

Connect to the required database and insert a document in the collections as shown bellow
```

__Connecting to Mongoose:__

```
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/mykoa');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
        console.log('connected to mongodb');
});

module.exports = db;
```

__Mongoose Schemas:__

var mongoose = require('mongoose'); 
var Schema = mongoose.Schema;  

var pages = new Schema({
  name: String,
  title: String,
  content: String,
  ru: [{
    title: String,
      content: String
   }],
   en: [{
    title: String,
      content: String
   }],
  active: Boolean,
  updated: { type: Date, default: Date.now }
});

```
__DB Migration:__

1. To dump your database for backup you call this command on your terminal
mongodump --db database_name --collection collection_name

2. To import your backup file to mongodb you can use the following command on your terminal
mongorestore --db database_name path_to_bson_file

NOTE: you can find "bson_files" in root .zip folder "mykoa.zip"

Example: (Windows)
Migrate db
  C:\Program Files\MongoDB\Server\3.2\bin>mongorestore --db mykoa C:\Program Files\MongoDB\Server\3.2\bin\dump\mykoa\mykoa.bson
Migrate tables
  C:\Program Files\MongoDB\Server\3.2\bin>mongorestore --db mykoa "C:\Program Files\MongoDB\Server\3.2\bin\dump\mykoa\pages.bson"

.. in other case, you can use 2 MongoDb insert queries:
1. db.pages.insert({ name: "", en: [{
title: "Home Page",
content: "Welcome to Koa website. It is great experience to learn it."
}], ru: [{
title: "Домашняя Страница",
content: "Добро пожаловать на КОА вебсайт. Это был прекрасный урок."
}]
})

2. db.pages.insert({ name: "aboutus", en: [{
title: "About Us",
content: "We are a great company producing different products. It is all started from an idea which transformed into the reality"
}], ru: [{
title: "Про нас",
content: "Мы очень большая компания на сегодняшний день. Все началось с маленькой идеи, которая потом преобразовалась в реальную жизнь."
}]
})

```
