var pages  = require('../models/pages');

module.exports = function() {

	this.showPage = function*() {
		let pageUrl = this.path.replace("/", "");
		let result = yield pages.findOne({'name': pageUrl});
				
		let lang = this.query.lang;

		if (!result)
		    this.throw(404, 'Page not found');
		
		this.body = yield this.render('index', {page: result, lang: lang})
	}

}