var pages  = require('../models/pages');

module.exports = function() {

	this.showHomePage = function*() {
		let result = yield pages.findOne({'name': ''});

		let lang = this.query.lang;

		//console.log(result);
		if (!result)
		    this.throw(404, 'Page not found');
		
		this.body = yield this.render('index', {page: result, lang: lang})
	}

}
