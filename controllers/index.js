var router  = require('koa-router')(),
    db	    = require('../config/mongo.js'),
    Home	= require('./home')
    Pages	= require('./pages')

module.exports = exports = function (app) {
	'use strict';
	
	var home = new Home();
	var pages = new Pages();

	// middleware
	app.use(router.routes())
	app.use(router.allowedMethods())
	
	// render page
	router.get('/', home.showHomePage) // home page
	router.get('/home', home.showHomePage) // home page
	router.get('/aboutus', pages.showPage) // any page
};
