'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var pages = new Schema({
	name: String,
	ru: [{
		title: String,
	    content: String
	 }],
	 en: [{
	 	title: String,
	    content: String
	 }],
	active: Boolean,
	updated: { type: Date, default: Date.now }
});

module.exports = mongoose.model('pages', pages);

